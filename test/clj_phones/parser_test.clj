(ns clj-phones.parser-test
  (:require [clojure.test :refer :all]
            [clj-phones.parser :refer :all]))

(deftest test-parser
  (testing "parse-us-number"
    (is (= "11234567890" (parse-us-number "(123) 456-7890")))
    (is (= nil (parse-us-number "(123)456-7890"))))
    
  (testing "parse-e164-number"
    (is (= "1234567890" (parse-e164-number "+1234567890")))
    (is (= nil (parse-e164-number "1234567890"))))
    
  (testing "parse-number"
    (is (= "11234567890" (parse-number "(123) 456-7890")))
    (is (= nil (parse-number "(123)456-7890")))
    (is (= "1234567890" (parse-number "+1234567890")))
    (is (= nil (parse-number "1234567890"))))
    
  (testing "build-context-dictionary"
    (let [entries [{:number 111, :name "rover", :context "work"}
                   {:number 111, :name "red", :context "personal"}]]
      (is (= {"work" "rover", "personal" "red"})))))
