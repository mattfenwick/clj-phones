(ns clj-phones.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [clj-phones.handler :refer :all]))

(deftest test-app
  (testing "parse-request-number"
    (is (= (parse-request-number "") nil))
    (is (= (parse-request-number "+123") "123"))
    (is (= (parse-request-number "abc") nil))
    (is (= (parse-request-number "+23947234") "23947234")))

  (testing "GET /query -- missing number"
    (let [response (app (mock/request :get "/query"))]
      (is (= (:status response) 400))
      (is (= (:body response) "{\"error\":\"missing or invalid number: \"}"))))

  (testing "GET /query -- invalid number"
    (let [response (app (mock/request :get "/query?number=111"))]
      (is (= (:status response) 400))
      (is (= (:body response) "{\"error\":\"missing or invalid number: 111\"}"))))

  (testing "GET /query -- success"
    (let [response (app (mock/request :get "/query?number=%2B1234567890"))]
      (is (= (:status response) 200))
      (is (= (:body response) "{\"results\":[]}"))))

  (testing "POST /number missing info"
    (let [response (app (mock/request :post "/number"))]
      (is (= (:status response) 400))))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))
