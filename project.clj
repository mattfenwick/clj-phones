(defproject clj-phones "0.1.0-SNAPSHOT"
  :description "A phones example"
  :url "https://bitbucket.org/mattfenwick/clj-phones"
  :min-lein-version "2.0.0"
  :main clj-phones.handler
  :aot [clj-phones.handler]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.5.1" :exclusions [ring/ring-core]]
                 [ring "1.6.2"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]]
  :resource-paths ["resources"]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler clj-phones.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
