(ns clj-phones.parser
  (:require [clojure.string :as string]
            [clojure.java.io :as io]))



(defn nil-join
  "Like join, but returns nil when collection is empty"
  [separator collection]
  (if (empty? collection)
      nil
      (string/join separator collection)))

(defn parse-us-number
  [number-string]
  (let [parsed (re-matches #"\((\d+)\) (\d+)-(\d+)" number-string)]
    (if (nil? parsed)
        nil
        (string/join "" (cons "+1" (rest parsed))))))

(defn parse-e164-number
  [number-string]
  (second (re-matches #"(\+\d+)" number-string)))

(defn parse-number
  [number-string]
  (or
    (parse-us-number number-string)
    (parse-e164-number number-string)
    nil))

(defn parse-line
  [line]
  (let [[number-string context name] (string/split line #",")
        number (parse-number number-string)]
    (if (empty? number)
        (throw (Exception. (str "invalid phone number: " number-string))) 
        {:phone number
         :context context
         :name name})))

(defn parse-data
  []
  (with-open [rdr (io/reader (clojure.java.io/resource "callerid-data.csv"))]
    (map parse-line (doall (line-seq rdr)))))

(defn build-context-dictionary
  [line-dicts]
  (zipmap (map :context line-dicts) (map :name line-dicts)))

(defn fmap
  [f m]
  (into {} (for [[k v] m] [k (f v)])))

(defn read-initial-data
  []
  (let [parsed-lines (parse-data)]
    (fmap build-context-dictionary (group-by :phone parsed-lines))))

