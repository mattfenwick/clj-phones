(ns clj-phones.handler
  (:gen-class)
  (:require [compojure.core :as core]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [clj-phones.parser :as parser]
            [ring.middleware.json :as json]
            [ring.adapter.jetty :as jetty]
            [ring.util.response :as response]))


; mutable state + model

(def numbers-atom (atom {}))

(defn insert-new-context
  [contexts new-context name]
  (if
    (nil? contexts)
    {new-context name}
    (into contexts [[new-context name]])))

(defn insert-new-record
  [numbers number context name]
  (into numbers [[number (insert-new-context (numbers number) context name)]]))

(defn create-new-record
  [number context name]
  (swap! numbers-atom #(insert-new-record % number context name)))

; request parsing

(defn parse-request-number
  [number-string]
  (if (nil? number-string)
      nil
      (let [parsed (re-matches #"(\+\d+)" number-string)]
        (second parsed))))

; routes

(core/defroutes app-routes

  (core/GET "/query" [number]
    (try
      (let [parsed-number 
            (or (parse-request-number number) 
                (throw (Exception. (str "missing or invalid number: " number))))]
        {:status 200
         :body {:results
                (map (fn [pair]
                       {:name (second pair)
                        :number number
                        :context (first pair)})
                     (@numbers-atom number))}})
      (catch Exception e {:status 400 :body {:error (.getMessage e)}})))

  (core/POST "/number" request
    (try
      (let [name (or (get-in request [:body :name]) 
                     (throw (Exception. "missing 'name'")))
            number-string (or (get-in request [:body :number]) 
                              (throw (Exception. "missing 'number'")))
            number (or (parse-request-number number-string) 
                       (throw (Exception. (str "invalid number format: " number-string))))
            context (or (get-in request [:body :context]) 
                        (throw (Exception. "missing 'context'")))]
        (create-new-record number context name)
        {:status 201})
      (catch Exception e {:status 400 :body {:error (.getMessage e)}})))

  (route/not-found {:status 404}))


; app

(def app
  (do
    (swap! numbers-atom #(into % (parser/read-initial-data)))
    (-> (handler/site app-routes)
        (json/wrap-json-body {:keywords? true})
        json/wrap-json-response)))

(defn parse-port
  [port-string]
  (try
    (Integer/parseInt port-string)
    (catch Exception e 3100)))

(defn -main
  [& args]
  (let [port (parse-port (first args))]
    (jetty/run-jetty
      app
      {:port port})))

