#! /bin/bash

# gets

echo 'GET /query?number=%2B123'
curl -X GET http://localhost:3000/query?number=%2B123
echo ''

echo 'GET /query?number=%2B18167228589'
curl -X GET http://localhost:3000/query?number=%2B18167228589
echo ''

echo 'GET missing number'
curl -X GET http://localhost:3000/query
echo ''

echo 'GET invalid number format'
curl -X GET http://localhost:3000/query?number=abc
echo ''

# posts

echo 'POST /number bad format ...'
curl -X POST http://localhost:3000/number --data '{"name":"Zekie", "number": "111-111-1111", "context": "personal"}' --header "Content-type:application/json"
echo ''

echo 'POST /number ...'
curl -X POST http://localhost:3000/number --data '{"name":"Dave", "number": "+123", "context": "personal"}' --header "Content-type:application/json"
echo ''

echo 'POST /number ... missing name'
curl -X POST http://localhost:3000/number --data '{"number": "123", "context": "personal"}' --header "Content-type:application/json"
echo ''

# echo 'POST /number ... missing name -- get status code
# curl  -X POST http://localhost:3000/number --data '{"number": "123", "context": "personal"}' --header "Content-type:application/json" -s -o /dev/null -w "%{http_code}"
# echo ''

# redo a get from before

echo 'GET /query?number=%2B123'
curl -X GET http://localhost:3000/query?number=%2B123
echo ''

