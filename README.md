# clj-phones

A mini server.


## Prerequisites

Install [leiningen](https://github.com/technomancy/leiningen) 2.0.0 or above.


## Running

To start a web server for the application, run:

    lein run

Or:

    lein run 3000

to specify a port.


## Building

    lein uberjar


## Running the uberjar

    java -jar target/clj-phones-0.1.0-SNAPSHOT-standalone.jar 3000


## Testing

    lein test
    
Or a few examples:

    bash tests.sh


## Using ring defaults:

    lein ring server

And:

    lein ring uberjar
